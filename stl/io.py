#!/usr/bin/env python3

from .solid import Solid


class StlSyntaxError(SyntaxError):
    def __init__(self, file, line, what):
        SyntaxError.__init__(
            self,
            "{} Line {}: {}".format(file, line, what)
            )


def read_text(filename):
    """
    read_text_stl(filename):

        Read a STL file.

    Parameters
        filename: the STL file name

    Return
        a list of Solid
    """
    S_SOLID, S_FACET, S_OUTER_LOOP, \
        S_VERTEX_1, S_VERTEX_2, S_VERTEX_3, \
        S_END_LOOP, S_END_FACET, S_END_SOLID = range(0, 9)
    solids = []
    with open(filename) as fin:
        vertices = []
        normals = []
        vertex = []
        normal = []
        solid_name = None
        line_counter = 0
        state = S_SOLID
        for line in fin:
            line_counter += 1
            words = line.split()
            words_num = len(words)
            if words_num == 0:
                continue
            elif state == S_SOLID:
                state += 1
                if words_num != 2 or words[0] != "solid":
                    raise StlSyntaxError(
                        filename, line_counter,
                        "invalid solid command."
                        )
                solid_name = words[1]
                vertices = []
                normals = []
            elif state == S_FACET:
                if words_num == 5 \
                        and words[0] == "facet" and words[1] == "normal":
                    state += 1
                    normal = [float(i) for i in words[2:]]
                elif words_num == 2 and words[0] == "endsolid":
                    state = S_SOLID
                    if solid_name != words[1]:
                        raise StlSyntaxError(
                            filename, line_counter,
                            "Name of solid {} does not match "
                            "the endsolid {}".format(
                                solid_name, words[1]
                                )
                            )
                    solids.append(Solid(vertices, normals, solid_name))
                    solid_name = None
                else:
                    raise StlSyntaxError(
                        filename, line_counter,
                        "expect facet or endsolid."
                        )
            elif state == S_OUTER_LOOP:
                state += 1
                vertex = []
                if words_num != 2 or \
                        words[0] != "outer" or words[1] != "loop":
                    raise StlSyntaxError(
                        filename, line_counter,
                        "expect outer loop."
                        )
            elif state >= S_VERTEX_1 and state <= S_VERTEX_3:
                state += 1
                if words_num != 4 or words[0] != "vertex":
                    raise StlSyntaxError(
                        filename, line_counter,
                        "expect a vertex."
                        )
                vertex += [float(i) for i in words[1:]]
            elif state == S_END_LOOP:
                state += 1
                if words_num != 1 or words[0] != "endloop":
                    raise StlSyntaxError(
                        filename, line_counter,
                        "expect endloop."
                        )
            elif state == S_END_FACET:
                state = S_FACET
                if words_num != 1 or words[0] != "endfacet":
                    raise StlSyntaxError(
                        filename, line_counter,
                        "expect endfacet."
                    )
                normals += normal
                vertices += vertex
            else:
                raise StlSyntaxError(
                    filename, line_counter,
                    "Invalid content: {}".format(line)
                )
    return solids
