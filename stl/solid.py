#!/usr/bin/env python3

import numpy as np


def merge_bounding_boxes(bbs):
    return (
        (
            min([n[0] for n, _ in bbs]),
            min([n[1] for n, _ in bbs]),
            min([n[2] for n, _ in bbs])
        ), (
            max([x[0] for _, x in bbs]),
            max([x[1] for _, x in bbs]),
            max([x[2] for _, x in bbs])
        )
    )


class Solid(object):
    name = None
    normals = None
    vertices = None
    num_triangles = 0

    def __init__(self, vertices, normals=None, name=None):
        self.vertices = np.array(vertices).reshape([-1, 3])
        if normals is not None:
            self.normals = np.array(normals).reshape([-1, 3])
        # should we check the rotation of normal?
        self.name = name
        if normals is not None and \
                self.normals.shape[0]*3 != self.vertices.shape[0]:
            raise ValueError(
                "Dimension of normals and vertices should be the same"
                )
        self.num_triangles = self.normals.shape[0]

    def bounding_box(self):
        return (
            np.min(self.vertices[:, 0]),
            np.min(self.vertices[:, 1]),
            np.min(self.vertices[:, 2])
        ), (
            np.max(self.vertices[:, 0]),
            np.max(self.vertices[:, 1]),
            np.max(self.vertices[:, 2])
        )

    def triangles(self):
        for i in range(self.num_triangles):
            yield self.vertices[i*3:i*3+3, :]

    def area(self, index=None):
        """
        Parameter
            index: the index of triangle element, start from 0,
                   if None then the sum will be calculated.
        Return the area as a float
        """
        assert index is None or index < self.num_triangles
        area = 0.0
        if index is not None:
            raise NotImplemented
        for t in self.triangles():
            v1 = t[1, :] - t[0, :]
            v2 = t[2, :] - t[0, :]
            area += np.linalg.norm(np.cross(v1, v2))
        return area/2
