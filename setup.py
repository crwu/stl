#!/usr/bin/env python3

import os
from setuptools import setup


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="stl",
    version="dev",
    description="A module that manipulate STL files.",
    long_description=read("readme.md"),
    author="Chuanren Wu",
    author_email="me@crwu.de",
    license="MIT",
    url="https://bitbucket.org/crwu/stl",
    packages=["stl"],
    install_requires=[
        "numpy",
    ]
)
