# Why another STL manipulation library?
For the model and mesh manipulation in CST, STL is the most common and simple format to handle.

However, the existing STL libraries do not meet my needs:
 - [Pystl](https://github.com/lwanger/pystl) just has a writer.
 - [Python-stl](https://github.com/apparentlymart/python-stl) lacks of numpy
 support.
 - Even though [numpy-stl](https://github.com/WoLpH/numpy-st) can handle
 multiple solids, it is not such convenient to adapt it for my usage.

# Dependency
 - [numpy](http://www.numpy.org/)


# License
This software is under [MIT License](license.txt) available.
