#!/usr/bin/env python3

from stl.io import read_text
from stl import merge_bounding_boxes
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection

fig = plt.figure()
ax = fig.add_subplot(111, projection="3d")
solids = read_text("test-data/torus_sphere.stl")
bbmin, bbmax = merge_bounding_boxes([i.bounding_box() for i in solids])
for s in solids:
    ax.add_collection3d(Poly3DCollection(list(s.triangles()), edgecolor='k'))

r = max([a-b for a, b in zip(bbmax, bbmin)])/2
cx = (bbmin[0]+bbmax[0])/2
cy = (bbmin[1]+bbmax[1])/2
cz = (bbmin[2]+bbmax[2])/2
ax.set_xlim3d([cx-r, cx+r])
ax.set_ylim3d([cy-r, cy+r])
ax.set_zlim3d([cz-r, cz+r])
plt.show()
