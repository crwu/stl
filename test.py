#!/usr/bin/env python3

import unittest
from stl.io import read_text
from tempfile import NamedTemporaryFile


def _missline(name_in, name_out, deletes):
    lc = 0
    with open(name_in) as fin:
        with open(name_out, "wt") as fout:
            for line in fin:
                lc += 1
                if lc not in deletes:
                    fout.write(line)


class Test(unittest.TestCase):
    def test_read_stl_text(self):
        solids = read_text("test-data/torus_sphere.stl")
        self.assertEqual(2, len(solids))
        self.assertEqual("Solid:Torus", solids[0].name)
        self.assertEqual("Solid:Sphere", solids[1].name)

    def test_read_stl_missing_line(self):
        filename = "test-data/torus_sphere.stl"
        with NamedTemporaryFile(buffering=0, delete=False) as f:
            for i in [j for j in
                      [i for i in (range(1, 16), range(22417, 22425), [27514])]
                      ]:
                _missline(filename, f.name, [1])
                self.assertRaises(SyntaxError, lambda: read_text(f.name))

    def test_area(self):
        solids = read_text("test-data/flat.stl")
        self.assertAlmostEqual(49.823658, solids[0].area(), places=5)


if __name__ == "__main__":
    unittest.main()
